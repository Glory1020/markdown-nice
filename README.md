<h1 align="center">Markdown Nice</h1>

## 简介

- 支持自定义样式的 Markdown 编辑器
- 支持微信公众号、知乎和稀土掘金

## 主题

> 已在项目的src\json目录下添加localThemeList.json用于定义本地主题

## 使用

### 1. 安装项目依赖
`npm install`

### 2. 启动项目
`npm start`

### 3. 访问
*http://localhost:3000*

## 友情链接

